/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 */

#include "cats.h"


#include <coap3/coap.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

static struct cats_screen cats_scr;

char g_addr[256];
char g_port[256];

LV_IMG_DECLARE(logo_img);
LV_IMG_DECLARE(key_0);
LV_IMG_DECLARE(key_1);
LV_IMG_DECLARE(key_2);
LV_IMG_DECLARE(key_3);
LV_IMG_DECLARE(key_4);
LV_IMG_DECLARE(key_5);
LV_IMG_DECLARE(key_6);
LV_IMG_DECLARE(key_7);
LV_IMG_DECLARE(key_8);
LV_IMG_DECLARE(key_9);
LV_IMG_DECLARE(key_asterisk);
LV_IMG_DECLARE(key_hash);

#define NUM_ROWS 4
#define NUM_COLS 3

static const lv_coord_t col_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static const lv_coord_t row_dsc[] = {
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_FR(1),
	LV_GRID_TEMPLATE_LAST
};

static const lv_img_dsc_t *key_imgs[NUM_ROWS][NUM_COLS] = {
	{ &key_1, &key_2, &key_3 },
	{ &key_4, &key_5, &key_6 },
	{ &key_7, &key_8, &key_9 },
	{ &key_asterisk, &key_0, &key_hash },
};

static char *data[NUM_ROWS][NUM_COLS] = {
	{ "1", "2", "3" },
	{ "4", "5", "6" },
	{ "7", "8", "9" },
	{ "*", "0", "#" },
};

static lv_style_t pressed_style;

int
resolve_address(const char *host, const char *service, coap_address_t *dst) {

	struct addrinfo *res, *ainfo;
	struct addrinfo hints;
	int error, len=-1;

	memset(&hints, 0, sizeof(hints));
	memset(dst, 0, sizeof(*dst));
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_family = AF_UNSPEC;

	error = getaddrinfo(host, service, &hints, &res);

	if (error != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
		return error;
	}

	for (ainfo = res; ainfo != NULL; ainfo = ainfo->ai_next) {
		switch (ainfo->ai_family) {
			case AF_INET6:
			case AF_INET:
				len = dst->size = ainfo->ai_addrlen;
				memcpy(&dst->addr.sin6, ainfo->ai_addr, dst->size);
				goto finish;
			default:
				;
		}
	}

finish:
	freeaddrinfo(res);
	return len;
}

static void on_button_pressed(lv_event_t *event)
{
	char *key = (char *)lv_event_get_user_data(event);
	fprintf(stderr, "%s button pressed\n", key);

	coap_context_t  *ctx = NULL;
	coap_session_t *session = NULL;
	coap_address_t dst;
	coap_pdu_t *pdu = NULL;
	int result = EXIT_FAILURE;;
	const uint8_t *payload = key;
	int ret = 0;

	coap_startup();

	/* Set logging level */
	coap_set_log_level(LOG_WARNING);
        
	/* resolve destination address where server should be sent */
	if (resolve_address(g_addr, g_port, &dst) < 0) {
		coap_log(LOG_CRIT, "failed to resolve address\n");
		goto finish;
	}

	/* create CoAP context and a client session */
	if (!(ctx = coap_new_context(NULL))) {
		coap_log(LOG_EMERG, "cannot create libcoap context\n");
		goto finish;
	}
	/* Support large responses */
	coap_context_set_block_mode(ctx,
					COAP_BLOCK_USE_LIBCOAP | COAP_BLOCK_SINGLE_BODY);
	if (!(session = coap_new_client_session(ctx, NULL, &dst, COAP_PROTO_UDP))) {
		coap_log(LOG_EMERG, "cannot create client session\n");
		goto finish;
	}

	/* construct CoAP message */
	pdu = coap_pdu_init(COAP_MESSAGE_NON,
						COAP_REQUEST_CODE_PUT,
						coap_new_message_id(session),
						coap_session_max_pdu_size(session));
	if (!pdu) {
		coap_log( LOG_EMERG, "cannot create PDU\n" );
		goto finish;
	}

	coap_add_data(pdu, 1, payload);

	coap_show_pdu(LOG_WARNING, pdu);

	/* and send the PDU */
	ret = coap_send(session, pdu);

	if(ret == 0) {
		printf("Error while sending packet\n");
	}

	result = EXIT_SUCCESS;
	finish:

	coap_session_release(session);
	coap_free_context(ctx);
	coap_cleanup();

	return;
}


void register_keypad_screen()
{
	lv_obj_t *scr, *keypad, *btn, *logo;
	int i, j;

	strcpy(g_port, "5683");
	strcpy(g_addr, "ff02::1");

	lv_style_init(&pressed_style);
	lv_style_set_img_recolor_opa(&pressed_style, LV_OPA_30);
	lv_style_set_img_recolor(&pressed_style, lv_color_white());

	lv_theme_default_init(NULL,
			      lv_palette_main(LV_PALETTE_BLUE),
			      lv_palette_main(LV_PALETTE_RED),
			      true,
			      &lv_font_montserrat_14);

	scr = lv_obj_create(NULL);
	lv_obj_set_size(scr, 600, 1024);
	lv_obj_align(scr, LV_ALIGN_CENTER, 0, 0);

	logo = lv_img_create(scr);
	lv_img_set_src(logo, &logo_img);
	lv_obj_align(logo, LV_ALIGN_TOP_MID, 0, 0);

	keypad = lv_obj_create(scr);
	lv_obj_set_style_grid_column_dsc_array(keypad, col_dsc, 0);
	lv_obj_set_style_grid_row_dsc_array(keypad, row_dsc, 0);
	lv_obj_set_size(keypad, 600, 800);
	lv_obj_set_layout(keypad, LV_LAYOUT_GRID);
	lv_obj_align(keypad, LV_ALIGN_BOTTOM_MID, 0, 0);

	for (i = 0; i < NUM_ROWS; i++) {
		for (j = 0; j < NUM_COLS; j++) {
			btn = lv_imgbtn_create(keypad);
			lv_imgbtn_set_src(btn, LV_IMGBTN_STATE_RELEASED,
					  NULL, key_imgs[i][j], NULL);
			lv_obj_add_style(btn, &pressed_style, LV_STATE_PRESSED);
			lv_obj_add_event_cb(btn, on_button_pressed,
					    LV_EVENT_CLICKED, (void *) data[i][j]);
			lv_obj_set_size(btn, 180, 180);
			lv_obj_set_grid_cell(btn,
					     LV_GRID_ALIGN_SPACE_EVENLY, j, 1,
					     LV_GRID_ALIGN_SPACE_EVENLY, i, 1);
		}
	}

	cats_scr.name = "keypad";
	cats_scr.obj = scr;
	cats_scr.orientation = CATS_SCREEN_HORIZONTAL;
	cats_add_screen(&cats_scr);
}
